/************************************************************************************************************
 * 2017
 * Michel Martinez
 * Pablo Diehl
 ************************************************************************************************************/

'use strict';

/*
 * Esta função salva os regitros da seção na memória persistente do aplicativo
 */
function salvaNaMemoria(){

}

//Este eventListener efetua as ações necessárias logo no carregamento da página
document.addEventListener('DOMContentLoaded', function () {
    //Carregamos a memória temporaria
    localforage.getItem('temporaria', function(err, temporaria) {
        //Depois buscamos os dados persistentes do condomínio
        console.log(temporaria);
    });


    /*AQUI VAMOS INSERIR DADOS TAMBÉM NA MEMÓRIA 'PERSISTENTE', APENAS PARA TESTES @MICHELMARTINEZ!!!!!!!!!!!!!!!
    localforage.getItem('persistente', function(err, persistente) {
        if(err) {
            console.log(err);
        } else {
            let condominio_input = document.getElementById('condominio').value,
                condominio_md5   = MD5(condominio_input.replace(/\s/g, '').toLowerCase());

            if(persistente === null) {
                persistente = {
                    lista : {
                        condominios : [condominio_input],
                        md5   : [condominio_md5]
                    }
                }
            } else {
                //Verifica se o item já existe no array, se não nem precisa editar
                if(persistente.lista.md5.indexOf(condominio_md5) === -1){
                    persistente.lista.condominios.push(condominio_input);
                    persistente.lista.md5.push(condominio_md5);
                }
            }

            localforage.setItem('persistente', persistente).then(function (valor) {
                console.log('[persistente_condominios]', valor);
            }).catch(function(err) {
                console.log('[persistente_condominios]', err);
            });
        }
    });*/
});


//Este eventListener adiciona novos inputs de apartamentos - Aqui usamos o umbrellajs
u(document.getElementById('mais')).on('click', function(){
    let number = u(document.getElementsByClassName('linha')).length;
    u(document.getElementById('aptos')).append('<div class="columns linha"><div class="column"><input id="apa' + number + '" class="input" type="text" placeholder="Nome do apartamento"></div><div class="column"><input id="ant' + number + '" class="input" type="text" placeholder="Leitura Anterior"></div><div class="column"><input id="atu' + number + '" class="input" type="text" placeholder="Leitura Atual"></div></div><hr>');
});


/* Este eventListener realiza as ações necessárias quando o usuário clica no botão 'próximo' */
document.getElementById('proximo').addEventListener('click', function(){
    //Verificamos se existe algum input
    if(u(document.getElementsByClassName('input')).length === 0){
        //Se não existe, mandamos um alerta
        iziToast.show({
            title: 'Atenção!',
            message: 'É necessário incluir pelo menos um apartamento para continuar!',
            position: 'bottomCenter'
        });
    } else {
        var salvar = true,
            quantidadeAptos = 0;

        //Se existe, verificamos os inputs
        let numeroLinhas = u(document.getElementsByClassName('linha')).length,
            i = 0,
            arrayAptos = [],
            aptosAcimaDoLimite = '',
            tamanhoAcimaDoLimite = 0;

        for(i=0; i < numeroLinhas; i++){
            let apa = document.getElementById('apa' + i).value,
                ant = document.getElementById('ant' + i).value,
                atu = document.getElementById('atu' + i).value;

            //Verifica se todos os inputs de uma linha foram
            if(apa.length > 0 && ant.length > 0 && atu.length > 0){
                //Verifica se os valores anterior e atual são números
                if(isNaN(ant) || isNaN(atu)){
                   salvar = false;
                } else {
                    let variacao = (atu - ant) *100 / ant;
                    if(variacao >= 40){
                        tamanhoAcimaDoLimite++;
                       aptosAcimaDoLimite += '"' + apa + '", ';
                    }
                    arrayAptos[i] = [apa, ant, atu];
                    quantidadeAptos++;
                }
            }
        }
        if(!salvar){
            iziToast.show({
                title: 'Atenção!',
                message: 'Valores de leitura anterior e leitura atual devem ser números!',
                position: 'bottomCenter'
            });
        } else {
            if(quantidadeAptos == 0){
                iziToast.show({
                    title: 'Atenção!',
                    message: 'É necessário preencher os dados de pelo menos um apartamento para continuar!',
                    position: 'bottomCenter'
                });
            } else {
                if(tamanhoAcimaDoLimite > 0){
                    let texto = tamanhoAcimaDoLimite == 1 ? 'O seguinte apartamento apresentou uma variação superior à 40%: ' : 'Os aparamentos seguintes aparamentos apresentaram uma variação de valores superior à 40%: ';
                    texto += aptosAcimaDoLimite.slice(0, -2);
                    iziToast.show({
                        title: 'Está correto?',
                        message: texto,
                        timeout: false,
                        position: 'bottomCenter',
                        buttons: [
                            ['<button>Sim, continue!</button>', function (instance, toast, aptos = arrayAptos) {
                                salvaNaMemoria(aptos);
                            }],
                            ['<button>Não, deixe-me editar os valores!</button>', function (instance, toast) {
                                instance.hide({
                                    transitionOut: 'fadeOutUp',
                                    onClosing: function(instance, toast, closedBy){}
                                }, toast, 'close', 'btn2');
                            }]
                        ],
                    });
                } else {
                    salvaNaMemoria(arrayAptos);
                }
            }
        }
    }
});
