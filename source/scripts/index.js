/************************************************************************************************************ 
 * 2017
 * Michel Martinez
 * Pablo Diehl
 ************************************************************************************************************/

'use strict';

// Este eventListener limpa os dados do localForage
document.getElementById('limpa-forage').addEventListener('click', function(){
    localforage.clear();
    iziToast.show({
        title: 'Sucesso!',
        message: 'A memória foi limpa.',
        position: 'bottomCenter'
    });
});

/* Este eventListener efetua as ações necessárias logo no carregamento da página */
document.addEventListener('DOMContentLoaded', function () {
    //Primeiramente, limpamos toda a memória temporaria
    localforage.removeItem('temporaria').then(function() {
        console.log('Memória temporária limpa!');
    }).catch(function(err) {
        console.log(err);
    });
    
    //Depois, verificamos se existe algum condomínio na memória
    localforage.getItem('persistente', function(err, persistente) {
        //Verificamos se ocorreu algum erro 
        if(err) {
            iziToast.show({
                title: 'Ops!',
                message: 'Ocorreu um problema ao tentar buscar dados na memória!\n Entre em contato com o administrador do sistema!',
                position: 'bottomCenter'
            });
            console.log(err);
        //Se não houver nenhum erro, precisamos verificar se existe algum registro na memória
        } else if(persistente !== null) {
            //Se tivermo condomínios, preenchemos o autocomplete com eles
            var input = document.getElementById('condominio'),
                awesomplete = new Awesomplete(input);

                awesomplete.list = persistente.lista.condominios;
        }
    });
});

/* Este eventListener realiza as ações necessárias quando o usuário clica no botão 'próximo' */
document.getElementById('proximo').addEventListener('click', function(){
    //Primeiro, verificamos se todos campos foram preenchidos
    if(document.getElementById('condominio').value.length == 0 || document.getElementById('data').value.length == 0){
        iziToast.show({
            title: 'Atenção!',
            message: 'É necessário preencher todos os campos do formulário para continuar!',
            position: 'bottomCenter'
        });
        return false; //Este return aqui é para encerrar a função, logo, nada abaixo será executado
    }
    
    //Montamos o objeto temporário
    let temporaria = {
        condominio:  document.getElementById('condominio').value,
        condominio_md5: MD5(document.getElementById('condominio').value),
        data: document.getElementById('data').value
    }
    
    //E armazenamos ele no Forage
    localforage.setItem('temporaria', temporaria).then(function (valor) {
        console.log('[temporaria]', valor);
        location.href = 'dois.html';
    //Se der algum erro, alertamos o usuário
    }).catch(function(err) {
        iziToast.show({
            title: 'Ops!',
            message: 'Ocorreu um problema ao tentar avançar para a próxima página!',
            position: 'bottomCenter'
        });
        console.log('[temporaria]', err);
    });
});
