/************************************************************************************************************ 
 * 2017
 * Michel Martinez
 * Pablo Diehl
 ************************************************************************************************************/

'use strict';


//Este eventListener efetua as ações necessárias logo no carregamento da página
document.addEventListener('DOMContentLoaded', function () {
    //Carregamos a memória temporaria
    localforage.getItem('temporaria', function(err, temporaria) {
        //Depois buscamos os dados persistentes do condomínio
        localforage.getItem('persistente', function(err, persistente) {
            //Verificamos se ocorreu algum erro 
            if(err) {
                iziToast.show({
                    title: 'Ops!',
                    message: 'Ocorreu um problema ao tentar buscar dados na memória!\n Entre em contato com o administrador do sistema!',
                    position: 'bottomCenter'
                });
                console.log(err);
            //Se não houver nenhum erro, precisamos verificar se existe algum registro na memória
            } else if(persistente !== null) {
                //Se tivermo condomínios, preenchemos o autocomplete com eles
                var input = document.getElementById('condominio'),
                    awesomplete = new Awesomplete(input);
                
                    awesomplete.list = persistente.condominios;
            }
        });
    });
});

/* Este eventListener realiza as ações necessárias quando o usuário clica no botão 'próximo' */
document.getElementById('proximo').addEventListener('click', function(){
    //Primeiro, verificamos se todos campos foram preenchidos
    if(document.getElementById('bloco').value.length == 0){
        //alert('Por favor, preencha todos os campos do formulário!');
        iziToast.show({
            title: 'Atenção!',
            message: 'É necessário informar um bloco para continuar!',
            position: 'bottomCenter'
        });
        return false; //Este return aqui é para encerrar a função, logo, nada abaixo será executado
    }
    
    //Recarregamos a memória temporária
    localforage.getItem('temporaria', function(err, temporaria) {
        temporaria.bloco = document.getElementById('bloco').value;
        temporaria.bloco_md5 = MD5(document.getElementById('bloco').value);

        //E a re-escrevemos com os dados novos no Forage
        localforage.setItem('temporaria', temporaria).then(function (valor) {
            console.log('[temporaria]', valor);
            location.href = 'tres.html';
        //Se der algum erro, alertamos o usuário
        }).catch(function(err) {
            iziToast.show({
                title: 'Ops!',
                message: 'Ocorreu um problema ao tentar avançar para a próxima página!',
                position: 'bottomCenter'
            });
            console.log('[temporaria]', err);
        });
    });
});
